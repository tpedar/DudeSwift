//: Playground - noun: a place where people can play

import UIKit

var movieRatings = ["Donnie Darko" : 4, "Chungking Express" : 5, "Dark City" : 4]
print("I have rated \(movieRatings.count) movies>")
let darkoRating = movieRatings["Donnie Darko"]
movieRatings["Dark City"] = 5
movieRatings
let oldRating: Int? = movieRatings.updateValue(5, forKey: "Donnie Darko")
if let lastRating = oldRating, currentRating = movieRatings["Donnie Darko"] {
    print("Old rating: \(lastRating); current rating: \(currentRating)")
}
movieRatings["The Cabinet of Dr. Calgari"] = 5
movieRatings.removeValueForKey("Dark City")
movieRatings["Dark City"] = nil
for (key, value) in movieRatings {
    print("The movie \(key) was rated \(value).")
}
for movie in movieRatings.keys {
    print("User has rated \(movie).")
}
let watchedMovies = Array(movieRatings.keys)

var Texas = ["Austin": [12345, 32145, 89320, 23178, 29018],
             "Arlen": [90190, 90210, 18910, 12810, 78210],
             "Kickapoo": [19020, 12123, 12345, 94949, 39202]
]
let zips = Array(Texas.values.flatten())
print("Texas has the following zip codes \(zips)")