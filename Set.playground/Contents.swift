//: Playground - noun: a place where people can play

import UIKit

//var groceryBag = Set<String>()
//groceryBag.insert("Apples")
//groceryBag.insert("Oranges")
//groceryBag.insert("Pineapple")
//var groceryBag = Set(["Apples", "Oranges", "Pineapple"])

var groceryBag: Set = ["Apples", "Oranges", "Pineapple"]
for food in groceryBag {
    print(food)
}


let hasBananas = groceryBag.contains("Bananas")

let friendGroceryBag: Set = ["Bananas", "Cereal", "Milk", "Oranges"]
let commonGroceryBag = groceryBag.union(friendGroceryBag)

let roommatesGroceryBag: Set = ["Apples", "Bananas", "Cereal", "Toothpaste"]
let itemsToReturn = commonGroceryBag.intersect(roommatesGroceryBag)

let yourSecondBag: Set = ["Berries", "Yogurt"]
let roommatesSecondBag: Set = ["Grapes", "Honey"]
let disjoint = yourSecondBag.isDisjointWith(roommatesGroceryBag)

print(disjoint)

//Bronze Challenge

//let myCities: Set = ["Atlanta", "Chicago", "Jacksonville", "New York", "San Francisco"]
//let yourCities: Set = ["Chicago", "San Francisco", "Jacksonville"]
//var areMyCitiesYourCities = myCities == yourCities
//print(areMyCitiesYourCities)

let myCities = Set(["Atlanta", "Chicago", "Jacksonville", "New York", "San Francisco"])
let yourCities = Set(["Chicago", "San Francisco", "Jacksonville"])
let isSuperset = myCities.isSupersetOf(yourCities)
print(isSuperset)


//Silver Challenge


groceryBag.unionInPlace(friendGroceryBag)

print(groceryBag)








