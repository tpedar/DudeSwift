//: Playground - noun: a place where people can play

import UIKit

enum TextAlignment: Int {
    case Left = 20
    case Right = 30
    case Center = 40
    case Justify = 50
}

var alignment = TextAlignment.Justify

print("Left has a raw value \(TextAlignment.Left.rawValue)")
print("Right has a raw value \(TextAlignment.Right.rawValue)")
print("Center has a raw value \(TextAlignment.Center.rawValue)")
print("Justify has a raw value \(TextAlignment.Center.rawValue)")
print("The alignment variable has a raw value \(alignment.rawValue)")

// Creat a rawValue
let myRawValue = 100

//Try to conver the raw value into a TextAlignment
if let myAlignment = TextAlignment(rawValue: myRawValue) {
    // Conversion Succeeded!
    print("successfully converted \(myRawValue) into a TextAlignment")
} else {
    // Conversion Failed
    print("\(myRawValue) has no corresponding TextAlignment case")
}
//alignment = .Right

//if alignment == .Right {
//    print("we should right-align the text!")
//}

switch alignment {
case .Left:
    print("left aligned")
    
case .Right:
    print("right aligned")
    
case .Center:
    print("center aligned")

case .Justify:
    print("justify aligned")
}


enum ProgrammingLanguage: String {
    case Swift
    case ObjectiveC = "Objective-C"
    case C
    case Cpp = "C++"
    case Java
}

let myFavoriteLanguage = ProgrammingLanguage.Swift
print("My favorite programing language is \(myFavoriteLanguage.rawValue)")

enum LightBulb {
    case On
    case Off
    
    func surfaceTemperatureForAmbientTemperature(ambient: Double) -> Double {
        switch self {
        case .On:
            return ambient + 150
            
        case .Off:
            return ambient
        }
    }
    
    mutating func toggle() {
        switch self {
        case .On:
            self = .Off
            
        case .Off:
            self = .On
        }
    }
}

var bulb = LightBulb.On
let ambientTemperature = 77.0

var bulbTemperature = bulb.surfaceTemperatureForAmbientTemperature(ambientTemperature)
print("the bulb's temperature is \(bulbTemperature)")

bulb.toggle()
bulbTemperature = bulb.surfaceTemperatureForAmbientTemperature(ambientTemperature)
print("the bulb's temperature is \(bulbTemperature)")


//enum ShapeDimensions {
//    //Point has no associated value - it is dimensionless
//    case Point
//    
//    //Square's associated value is the length of one side case Square(Double)
//    case Square(Double)
//    
//    //Rectangle's associated value defines its width and height
//    case Rectangle(width: Double, height: Double)
//    
//    func area() -> Double {
//        switch self {
//        case .Point:
//            return 0
//            
//        case let .Square(side):
//            return side * side
//            
//        case let .Rectangle(width: w, height: h):
//            return w * h
//        }
//    }
//}
//
//
//var squareShape = ShapeDimensions.Square(10.0)
//var rectShape = ShapeDimensions.Rectangle(width: 5.0, height: 10.0)
//var pointShape = ShapeDimensions.Point
//
//print("square's area = \(squareShape.area())")
//print("rectangle's area = \(rectShape.area())")
//print("point's area = \(pointShape.area())")


enum FamiliyTree {
    case NoKnownParents
    indirect case OneKnownParent(name: String, ancestors: FamiliyTree)
    indirect case TwoknownParents(fatherName: String, fatherAncestors: FamiliyTree, motherName: String, motherAncestors: FamiliyTree)
}

let fredAncestors = FamiliyTree.TwoknownParents(fatherName: "Fred Se", fatherAncestors: .OneKnownParent(name: "Beth", ancestors: .NoKnownParents), motherName: "Marsha", motherAncestors: .NoKnownParents)

// Bronze Challenge


enum ShapeDimensions {
    // Point has no associated vlaue - it is dimensionless
    case Point
    // Square's associated value is the length of one side
    case Square(Double)
    // Rectangle's associated value defines its width and height
    case Rectangle(width: Double, height: Double)
    case RightTriangle(a: Double, b: Double, c: Double)
    func area() -> Double {
        switch self {
        case .Point:
            return 0
        case let .Square(side):
            return side * side
        case let .Rectangle(width: w, height: h):
            return w * h
        case let .RightTriangle(a: a, b: b, c: _):
            return (a * b) / 2
        }
    }
    func perimeter() -> Double {
        switch self {
        case .Point:
            return 0
        case let .Square(side):
            return 4 * side
        case let .Rectangle(width: w, height: h):
            return 2 * (w + h)
        case let .RightTriangle(a: a, b: b, c: _):
            return a + b + sqrt(pow(a, 2) + pow(b, 2))
        }
    }
}
var squareShape = ShapeDimensions.Square(10.0)
var rectShape = ShapeDimensions.Rectangle(width: 5.0, height: 10.0)
var pointShape = ShapeDimensions.Point
var rightTriangle = ShapeDimensions.RightTriangle(a: 5.0, b: 5.0, c: 5.0)
print("square's area = \(squareShape.area())")
print("rectangle's area = \(rectShape.area())")
print("point's area = \(pointShape.area())")
print("point's area = \(rightTriangle.area())")
print("square's perimeter = \(squareShape.perimeter())")
print("rectangle's perimeter = \(rectShape.perimeter())")
print("point's perimeter = \(pointShape.perimeter())")
print("point's perimeter = \(rightTriangle.perimeter())")


















