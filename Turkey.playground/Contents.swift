//: Playground - noun: a place where people can play

import UIKit

var weight: Float

weight = 14.2

print("The turkey weighs \(weight)lbs.")

var cookingTime: Float

cookingTime = 15 + 15 * weight

print("Cook it for \(cookingTime) minutes.")
