//: Playground - noun: a place where people can play

import UIKit

func showCookTimeForTurkey(pounds: Int) {
    let necessaryMinutes = 15 + 15 * pounds
    print("Cook for \(necessaryMinutes) minutes")
    if (necessaryMinutes > 120) {
        let halfway: Int = necessaryMinutes / 2
        print("Rotate after \(halfway) of the \(necessaryMinutes) minutes")
    }
}


var totalWeight = 10
var gibletsWeight = 1
var turkeyWeight = totalWeight - gibletsWeight

showCookTimeForTurkey(turkeyWeight)


func singSongFor(numberOfBottles: Int) {
    if numberOfBottles == 0 {
        print("There are simply no more bottles of beer on the wall")
    } else {
        print("\(numberOfBottles) bottles of beer on the wall. \(numberOfBottles) bottles of beer.")
        let oneFewer = numberOfBottles - 1
        print("Take one down, pass it around, \(numberOfBottles) bottles of beer on the wall.")
        singSongFor(oneFewer)
        print("Put a bottle in the recycling, \(numberOfBottles) empty bottles in the bin")
    }
}


    singSongFor(4)
