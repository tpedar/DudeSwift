//: Playground - noun: a place where people can play

import UIKit

//var numberOfStoplights = "Four"

var numberOfStoplights: Int = 4

var population: Int

let townName: String = "Knowhere"

var employmentRate: Int = 3

numberOfStoplights += 2

population = 5422

print(numberOfStoplights)

let townDescription =
"\(townName) has a population of \(population) and \(numberOfStoplights) stoplights, it has a \(employmentRate)% employment rate."

print(townDescription)
