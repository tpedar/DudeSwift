//: Playground - noun: a place where people can play

import UIKit

var myFirstInt: Int = 0

for _ in 1...5 {
    myFirstInt += 1
    print(myFirstInt)
}

for case let i in 1...100 where i % 3 == 0 {
    print(i)
}

for i in 1...100 {
    if i % 3 == 0 {
        print(i)
    }
}

// Classis for loop

for i in 1 ..< 6 {
    myFirstInt += 1
    print(myFirstInt)
}

//While Loop
var i = 1
while i < 6 {
    myFirstInt += 1
    print(myFirstInt)
    i += 1
}

//var shields = 5
//var blastersOverheating = false
//var blasterFireCount = 0
//var spaceDemonsDestroyed = 0
//while shields > 0 {
//    
//    if spaceDemonsDestroyed == 500 {
//        print("You bet the game!")
//        break
//    }
//   
//    if blastersOverheating {
//        print("Blasters are overheated!  Cooldown initiated.")
//        sleep(5)
//        print("Blasters ready to fire")
//        sleep(1)
//        blastersOverheating = false
//        
//    }
//    
//    if blasterFireCount > 100 {
//        blastersOverheating = true
//        continue
//    }
//    // Fure Blasters!
//    print("Fire Blasters!")
//    
//    blasterFireCount += 1
//    spaceDemonsDestroyed += 1
//}


for i in 1...5 {
    print("\nOutside Loop Count: \(i)")
    sleep(5)
    for var x = 0; x <= 100; x+=2 {
        print("Inside Loop: \(x)")
    }
}


for _ in 1...5{
    for case let i in 0...100 where i % 2 == 0{print(i)}
}


