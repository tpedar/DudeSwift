//: Playground - noun: a place where people can play

import UIKit

var lastTemperature: Float = 0.0

func fahrenheitFromCelsius(cel: Float) -> Float {
    lastTemperature = cel
    let fahr: Float = cel * 1.8 + 32.0
    print("\(cel) Celsius is \(fahr) Fahrenheit.")
    return fahr
}


var freezeInC: Float = 0
var freezeInF: Float = fahrenheitFromCelsius(freezeInC)
print("Water freezes at \(freezeInF) degreees Fahrenheit.")
print("The last temperature converted was \(lastTemperature)")

    




