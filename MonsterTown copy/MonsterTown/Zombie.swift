//
//  Zombie.swift
//  MonsterTown
//
//  Created by Taylor Petersen on 7/5/16.
//  Copyright © 2016 BigNerdRanch. All rights reserved.
//

import Foundation

class Zombie: Monster  {
    override class var spookyNoise: String {
        return "Brains..."
    }
    var walksWithLimp = true
    private var isFallingApart = false
    final override func terrorizeTown() {
        if !isFallingApart {
        if town?.population > 0 {
            town?.changePopulation(-10)
            if town?.population < 0 {town?.population = 0}
            }
            super.terrorizeTown()
        }
    }
    func changeName(name: String, walksWithLimp: Bool){
        self.name = name
        self.walksWithLimp = walksWithLimp
    }
}