//
//  Town.swift
//  MonsterTown
//
//  Created by Taylor Petersen on 7/5/16.
//  Copyright © 2016 BigNerdRanch. All rights reserved.
//

import Foundation

struct Town {
    static let region = "South"
    var population = 5422 {
        didSet(oldPopulation) {
            print("The population had changed to \(population) from \(oldPopulation).")
        }
        
    }
    var numberOfStoplights = 4
    
    enum Size {
        case Small
        case Medium
        case Large
    }
    var townSize: Size {
        switch self.population {
        case 0...10000:
            return Size.Small
            
        case 10001...100000:
            return Size.Medium
            
        default:
            return Size.Large
        }
    }
    func printTownDescription() {
        print("Population: \(population); number of stoplights: \(numberOfStoplights)")
    }
    
    mutating func changePopulation(amount: Int) {
        population += amount
    }
}