//
//  Vampire.swift
//  MonsterTown
//
//  Created by Taylor Petersen on 7/5/16.
//  Copyright © 2016 BigNerdRanch. All rights reserved.
//

import Foundation

class Vampire: Monster {
    var vampires = [Vampire]()
    override func terrorizeTown() {
        if town?.population > 0 {
            let vampire = Vampire()
            vampires.append(vampire)
            town?.changePopulation(-1)
        } else {
            print("This town has no one to turn!")
        }
        super.terrorizeTown()
        print("There are \(vampires.count + 1) vampires in this town!")
    }
}