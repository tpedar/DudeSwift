//
//  main.swift
//  MonsterTown
//
//  Created by Taylor Petersen on 7/5/16.
//  Copyright © 2016 BigNerdRanch. All rights reserved.
//

import Foundation

var myTown = Town()
myTown.changePopulation(500)
//myTown.printTownDescription()
//let gm = Monster()
//gm.town = myTown
//gm.terrorizeTown()
let fredTheZombie = Zombie()
fredTheZombie.town = myTown
fredTheZombie.terrorizeTown()
fredTheZombie.town?.printTownDescription()
fredTheZombie.changeName("Fred the Zombie", walksWithLimp: false)
let frankTheVampire = Vampire()
frankTheVampire.town = myTown
frankTheVampire.terrorizeTown()
fredTheZombie.town?.printTownDescription()
