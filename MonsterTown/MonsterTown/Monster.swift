//
//  Monster.swift
//  MonsterTown
//
//  Created by Taylor Petersen on 7/5/16.
//  Copyright © 2016 BigNerdRanch. All rights reserved.
//

import Foundation

class Monster {
    var town: Town?
    var name = "Monster"
    
    func terrorizeTown() {
        if town != nil {
            print("\(name) is terrorizing a town!")
        } else {
            print("\(name) hasn't found a town to terrorize yet...")
        }
    }
}